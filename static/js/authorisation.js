function get(selector) {
    return document.querySelector(selector);
}

const entranceMenu = get("#author"); // document.querySelector('') var ->  let / const
const registeryIcon = get("#img2");
const enterBtn = get("#entrance");
const createBtn = get("#sign-up");

registeryIcon.addEventListener("click", () => {
    entranceMenu.style.display = 'block';
});
// enterBtn.onclick = () => {
//     console.log('works')
// }

enterBtn.addEventListener("click", () => {
    entranceMenu.style.display = 'none';
});

createBtn.addEventListener("click", () => {
    location.href = "/register";
});
// account entrance program
const email = localStorage.getItem("someEmail");
const password = localStorage.getItem("somePassword");
