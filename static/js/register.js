function get(selector) {
    return document.querySelector(selector);
}

const submitBtn = get("#register");
const hyperlink = get("#r-enter");
const entranceMenu = get("#author");

submitBtn.addEventListener("click", () => {
    const someEmail = get("#r-email").value;
    const somePassword = get("#r-password").value;
    const someLogin = get("#login").value;
    if (someEmail !== '' && someEmail.includes("@")) {
        console.log(somePassword);
        localStorage.setItem("someEmail", `${someEmail}`);
        localStorage.setItem("someLogin", `${someLogin}`);
        localStorage.setItem("somePassword", `${somePassword}`);
    }
});

hyperlink.addEventListener("click", () => {
    entranceMenu.style.display = 'block';
});